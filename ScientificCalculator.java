import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;
public class ScientificCalculator extends JFrame implements ActionListener {
    JTextField textField;
    double temp, temp1, result, a;
    static double m1, m2;
    int K = 1, X = 0, Y = 0, Z = 0;
    char ch;
    JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, zero, clr, pow2, pow3, exp, fac, plus, min, div, log, rec, mul, eq, addSub,
            dot, mr, mc, mp, mm, sqrt, sin, cos, tan;
    Container cont;
    JPanel textPanel, buttonPanel;

    ScientificCalculator() {
        cont = getContentPane();
        cont.setLayout(new BorderLayout());
        JPanel textPanel = new JPanel();
        textField = new JTextField(25);
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        textField.addKeyListener(new KeyAdapter() {
            public void KeyTyped(KeyEvent keyevent) {
                char c = keyevent.getKeyChar();
                if (c >= '0' && c <= '9') {
                } else {
                    keyevent.consume();
                }
            }
        });
        textPanel.add(textField);
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(8, 4, 2, 2));
        boolean t = true;
        mr = new JButton("MR");
        buttonPanel.add(mr);
        mr.addActionListener(this);

        mc = new JButton("MC");
        buttonPanel.add(mc);
        mc.addActionListener(this);

        mp = new JButton("M+");
        buttonPanel.add(mp);
        mp.addActionListener(this);

        mm = new JButton("M+");
        buttonPanel.add(mm);
        mm.addActionListener(this);

        b1 = new JButton("1");
        buttonPanel.add(b1);
        b1.addActionListener(this);

        b2 = new JButton("2");
        buttonPanel.add(b2);
        b2.addActionListener(this);

        b3 = new JButton("3");
        buttonPanel.add(b3);
        b3.addActionListener(this);

        b4 = new JButton("4");
        buttonPanel.add(b4);
        b4.addActionListener(this);

        b5 = new JButton("5");
        buttonPanel.add(b5);
        b5.addActionListener(this);

        b6 = new JButton("6");
        buttonPanel.add(b6);
        b6.addActionListener(this);

        b7 = new JButton("7");
        buttonPanel.add(b7);
        b7.addActionListener(this);

        b8 = new JButton("8");
        buttonPanel.add(b8);
        b8.addActionListener(this);

        b9 = new JButton("9");
        buttonPanel.add(b9);
        b9.addActionListener(this);

        zero = new JButton("0");
        buttonPanel.add(zero);
        zero.addActionListener(this);

        plus = new JButton("+");
        buttonPanel.add(plus);
        plus.addActionListener(this);

        min = new JButton("-");
        buttonPanel.add(min);
        min.addActionListener(this);

        mul = new JButton("*");
        buttonPanel.add(mul);
        mul.addActionListener(this);

        div = new JButton("/");
        buttonPanel.add(div);
        div.addActionListener(this);

        addSub = new JButton("+/-");
        buttonPanel.add(addSub);
        addSub.addActionListener(this);

        dot = new JButton(".");
        buttonPanel.add(dot);
        dot.addActionListener(this);

        eq = new JButton("=");
        buttonPanel.add(eq);
        eq.addActionListener(this);

        rec = new JButton("1/x");
        buttonPanel.add(rec);
        rec.addActionListener(this);

        sqrt = new JButton("SQRT");
        buttonPanel.add(sqrt);
        sqrt.addActionListener(this);

        log = new JButton("log");
        buttonPanel.add(log);
        log.addActionListener(this);

        sin = new JButton("SIN");
        buttonPanel.add(sin);
        sin.addActionListener(this);

        cos = new JButton("COS");
        buttonPanel.add(cos);
        cos.addActionListener(this);

        tan = new JButton("TAN");
        buttonPanel.add(tan);
        tan.addActionListener(this);

        pow2 = new JButton("x^2");
        buttonPanel.add(pow2);
        pow2.addActionListener(this);

        pow3 = new JButton("x^3");
        buttonPanel.add(pow3);
        pow3.addActionListener(this);

        exp = new JButton("EXP");
        buttonPanel.add(exp);
        exp.addActionListener(this);

        fac = new JButton("n!");
        buttonPanel.add(fac);
        fac.addActionListener(this);

        clr = new JButton("AC");
        buttonPanel.add(clr);
        clr.addActionListener(this);
        cont.add("Center", buttonPanel);
        cont.add("North", textPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        if (s.equals("1")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "1");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "1");
                Z = 0;
            }
        }
        if (s.equals("2")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "2");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "2");
                Z = 0;
            }
        }
        if (s.equals("3")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "3");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "3");
                Z = 0;
            }
        }
        if (s.equals("4")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "4");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "4");
                Z = 0;
            }
        }
        if (s.equals("5")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "5");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "5");
                Z = 0;
            }
        }
        if (s.equals("6")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "6");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "6");
                Z = 0;
            }
        }
        if (s.equals("7")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "7");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "7");
                Z = 0;
            }
        }
        if (s.equals("8")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "8");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "8");
                Z = 0;
            }
        }
        if (s.equals("9")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "9");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "9");
                Z = 0;
            }
        }
        if (s.equals("0")) {
            if (Z == 0) {
                textField.setText(textField.getText() + "0");
            } else {
                textField.setText("");
                textField.setText(textField.getText() + "0");
                Z = 0;
            }
        }
        if (s.equals("AC")) {
            textField.setText("");
            X = 0;
            Y = 0;
            Z = 0;
        }
        if (s.equals("log")) {
            if (textField.getText().equals("")) {
                textField.setText("");
            } else {
                a = Math.log(Double.parseDouble(textField.getText()));
                textField.setText("");
                textField.setText(textField.getText() + a);
            }
        }
        if (s.equals("1/x")) {
            if (textField.getText().equals("")) {
                textField.setText("");
            } else {
                a = 1 / (Double.parseDouble(textField.getText()));
                textField.setText("");
                textField.setText(textField.getText() + a);
            }
        }
        if (s.equals("Exp")) {
            if (textField.getText().equals("")) {
                textField.setText("");
            } else {
                a = Math.exp(Double.parseDouble(textField.getText()));
                textField.setText("");
                textField.setText(textField.getText() + a);
            }
        }
        if (s.equals("x^2")) {
            if (textField.getText().equals("")) {
                textField.setText("");
            } else {
                a = Math.pow(Double.parseDouble(textField.getText()), 2);
                textField.setText("");
                textField.setText(textField.getText() + a);
            }
        }
        if (s.equals("x^3")) {
            if (textField.getText().equals("")) {
                textField.setText("");
            } else {
                a = Math.pow(Double.parseDouble(textField.getText()), 3);
                textField.setText("");
                textField.setText(textField.getText() + a);
            }
        }
        if (s.equals("+/-")) {
            if (X == 0) {
                textField.setText("-" + textField.getText());
                X = 1;
            } else {
                textField.setText(textField.getText());
            }
        }
        if (s.equals(".")) {
            if(Y==0){
                textField.setText(textField.getText() + ".");
                Y = 1;
            }else{
                textField.setText(textField.getText());
            }
        }
       if(s.equals("+")){
           if(textField.getText().equals("")){
               textField.setText("");
               temp = 0;
               ch = '+';
           }else{
               X = 0;
               Y = 0;
               temp = Double.parseDouble(textField.getText());
               textField.setText("");
               ch = '+';
           }
           textField.requestFocus();
       }
       if(s.equals("-")){
           if(textField.getText().equals("")){
               textField.setText("");
               temp = 0;
               ch = '-';
           }else{
               X = 0;
               Y = 0;
               temp = Double.parseDouble(textField.getText());
               textField.setText("");
               ch = '-';
           }
           textField.requestFocus();
       }
       if(s.equals("*")) {
           if(textField.getText().equals("")){
               textField.setText("");
               temp = 1;
               ch = '*';
           }else{
               X = 0;
               Y = 0;
               temp = Double.parseDouble(textField.getText());
               textField.setText("");
               ch = '*';
           }
           textField.requestFocus();
       }
       if(s.equals("/")){
           if(textField.getText().equals("")){
               textField.setText("");
               temp = 1;
               ch = '/';
           }else{
               X = 0;
               Y = 0;
               temp = Double.parseDouble(textField.getText());
               textField.setText("");
               ch = '/';
           }
           textField.requestFocus();
       }
       if(s.equals("MC")){
           m1 = 0;
           textField.setText("");
       }
       if(s.equals("MR")){
           textField.setText("");
           textField.setText(textField.getText() + m1);
       }
       if(s.equals("M+")){
           if(K==1){
               m1 = Double.parseDouble(textField.getText());
               K++;
           }else{
               m1 += Double.parseDouble(textField.getText());
               textField.setText("" + m1);
           }
       }
       if(s.equals("M-")){
           if(K==1){
               m1 = Double.parseDouble(textField.getText());
               K++;
           }else{
               m1 -= Double.parseDouble(textField.getText());
               textField.setText("" + m1);
           }
       }
       if(s.equals("sqrt")){
           if(textField.getText().equals("")){
               textField.setText("");
           }else{
               a = Math.sqrt(Double.parseDouble(textField.getText()));
               textField.setText("");
               textField.setText(textField.getText() + a);
           }
       }
       if(s.equals("SIN")){
           if(textField.getText().equals("")){
               textField.setText("");
           }else{
               a = Math.sin(Double.parseDouble(textField.getText()));
               textField.setText("");
               textField.setText(textField.getText() + a);
           }
       }
       if(s.equals("COS")){
           if(textField.getText().equals("")){
               textField.setText("");
           }else{
               a = Math.cos(Double.parseDouble(textField.getText()));
               textField.setText("");
               textField.setText(textField.getText() + a);
           }
       }
       if(s.equals("TAN")){
           if(textField.getText().equals("")){
               textField.setText("");
           }else{
               a = Math.tan(Double.parseDouble(textField.getText()));
               textField.setText("");
               textField.setText(textField.getText() + a);
           }
       }
       if(s.equals("=")){
            if(textField.getText().equals("")){
                textField.setText("");
            }else{
                temp1 = Double.parseDouble(textField.getText());
                switch(ch){
                    case '+':
                        result = temp + temp1;
                        break;
                    case '-':
                        result = temp - temp1;
                        break;
                    case '/':
                        result = temp * temp1;
                        break;
                    case '*':
                        result = temp / temp1;
                        break;
                }
                textField.setText("");
                textField.setText(textField.getText() + result);
                Z = 1;
            }
       }
       if(s.equals("n!")){
           if(textField.getText().equals("")){
               textField.setText("");
           }else{
               a = fact(Double.parseDouble(textField.getText()));
               textField.setText("");
               textField.setText(textField.getText() + a);
           }
       }
       textField.requestFocus();
    }
    double fact(double X) {
        int er = 0;
        if (X < 0) {
            er = 20;

            return 0;
        }
        double i,s = 1;
        for(i = 2; i < X; i += 1.0)
            s*=i;
        return s;
    }
    public static void main(String args[]){
        try{
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }catch(Exception e){
        }
        ScientificCalculator f =new ScientificCalculator();
        f.setTitle("ScientificCalculator");
        f.pack();
        f.setVisible(true);
        }
    }
